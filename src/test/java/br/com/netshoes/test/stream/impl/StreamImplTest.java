package br.com.netshoes.test.stream.impl;

import org.junit.Test;

import br.com.netshoes.test.stream.Stream;
import org.junit.Assert;

public class StreamImplTest {
	
	
	public void metodoHasNextRetornaSomenteCaracteresParaProcessarSucesso() {
		Stream stream = new StreamImpl("aAbBABac");
		
		Assert.assertTrue("retorn true porque tem repeticao", stream.hasChar());
		Assert.assertTrue("retorn true porque tem repeticao", stream.hasChar());
		Assert.assertTrue("retorn true porque tem repeticao", stream.hasChar());
		Assert.assertTrue("retorn true porque tem repeticao", stream.hasChar());
		Assert.assertFalse("retorn false porque não tem mais repeticao", stream.hasChar());
	}
	
	@Test
	public void metodoGetNextRetornaSomenteCaracteresParaProcessarSucesso() {
		Stream stream = new StreamImpl("aAbBABac");

		Assert.assertTrue("primeiro caracter deve ser b", stream.getNext() == 'b');

		Assert.assertTrue("segundo caracter deve ser A", stream.getNext() == 'A');

		Assert.assertTrue("terceiro caracter deve ser B", stream.getNext() == 'B');
		
		Assert.assertTrue("quarto caracter deve ser a", stream.getNext() == 'a');
		
		Assert.assertTrue("quinto caracter deve ser c", stream.getNext() == 'c');	

	}
	
	@Test(expected=Exception.class)
	public void errorAoAcessarMetodoGetNextQueNãoTemMaisCaracter(){
		Stream stream = new StreamImpl("aaaaaaaa");
		stream.getNext();
	}
}
