package br.com.netshoes.test.stream;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import br.com.netshoes.test.stream.impl.StreamImpl;

public class NetshoesStreamTest {
	
	public static void main(String[] args) throws IOException {
		NetshoesStreamTest netshoesStreamTest = new NetshoesStreamTest();
		
		boolean continuar = true;
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));  
		
		Stream stream = netshoesStreamTest.getEntrada(br);
		while (continuar) {
			int opcao = netshoesStreamTest.opcoes();
			switch (opcao) {
			case 1:
				char c = netshoesStreamTest.firstChar(stream);
				if(c == Character.MIN_VALUE){
					System.out.println("Não há mais caracteres não repetidos");
				}else{
					System.out.println("Proximo Caracter é:"+c);	
				}				
				break;
			case 2:
				stream = netshoesStreamTest.getEntrada(br);
				break;
			case 3:
				continuar = false;
				break;
			default:
				System.out.println("Opção invalida.");
				break;
			}
			
		}

	}
	
	public Stream getEntrada(BufferedReader br) throws IOException{
		System.out.println("informe a entrada:");
		String entrada = br.readLine();
		return new StreamImpl(entrada);
	}
	
	public int opcoes() throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));  
		System.out.println("informe a opção desejada:");
		System.out.println("1 - Proximo caracter:");
		System.out.println("2 - informe informar nova entrada:");
		System.out.println("0 - Sair:");
		String opcao = br.readLine();
		
		if(opcao.equals("1")){
			return 1;
		}else if(opcao.equals("2")){
			return 2;
		}else  if(opcao.equals("0")){
			return 0;
		}
		return -1;
	}
	
	public char firstChar(Stream input){
		if(input.hasChar()){
			return input.getNext();
		}		
		return Character.MIN_VALUE;
	}

}
