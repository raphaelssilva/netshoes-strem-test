/**
 * 
 */
/**
 * @author raphael
 *
 */
package br.com.netshoes.test.stream.impl;

import br.com.netshoes.test.stream.Stream;

public class StreamImpl implements Stream{
	String texto;
	
	int proximoChar = 0;
	boolean rodouHasChar = false;
	
	public StreamImpl(String texto){
		this.texto = texto;
	}

	@Override
	public char getNext() {
		if(!rodouHasChar){
			proximoChar();
		}
		rodouHasChar = false;
		return texto.charAt(this.proximoChar);					
	}

	@Override
	public boolean hasChar() {
		rodouHasChar = true;
		proximoChar();
		if(this.proximoChar!=-1){
			return true;	
		}				
		return false;
	}
	
	private void proximoChar(){
		if(this.proximoChar>=0){
			String textoRestante = texto.substring(this.proximoChar).toUpperCase();
			for (int i = 1; i < textoRestante.length(); i++) {
				if(textoRestante.charAt(i) != textoRestante.charAt(i-1)){
					this.proximoChar = this.proximoChar+i;
					return;
				}
			}		
			this.proximoChar = -1;
		}				
	}
	
}