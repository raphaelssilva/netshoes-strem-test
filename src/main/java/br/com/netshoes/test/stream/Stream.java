/**
 * 
 */
/**
 * @author raphael
 *
 */
package br.com.netshoes.test.stream;

public interface Stream{
	public char getNext();
	public boolean hasChar();
}